# Import required modules
import cv2
import numpy as np
import sys
 
# Create a white canvas
def createCanvas(color):
    canvas = np.ones((640, 640, 3), dtype=np.uint8)
    return color * canvas;

# Display an image
def displayImage(window_name, image):
    cv2.imshow(window_name, image)

# Load the color palette
palette = cv2.imread('palette.jpg', cv2.IMREAD_COLOR)
if palette is None:
    sys.exit("Could not read the palette image.")

# Create the canvas
white = np.array([255, 255, 255], dtype=np.uint8)
canvas = createCanvas(white)

# Display the canvas and the color palette
displayImage('Color palette', palette)
displayImage('Canvas', canvas)
 
cv2.waitKey(0) 
cv2.destroyAllWindows()
